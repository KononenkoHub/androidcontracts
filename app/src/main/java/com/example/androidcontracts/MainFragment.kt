package com.example.androidcontracts

import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import coil.load
import coil.transform.CircleCropTransformation
import com.example.androidcontracts.databinding.FragmentMainBinding
import com.example.androidcontracts.extantions.viewBindings

class MainFragment : Fragment(R.layout.fragment_main) {
    private val binding by viewBindings(FragmentMainBinding::bind)
    private val pickImage =
        registerForActivityResult(ActivityResultContracts.GetContent()) { imageUri ->
            binding.image.load(imageUri) {
                crossfade(true)
                transformations(CircleCropTransformation())
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loadButton.setOnClickListener {
            pickImage.launch("image/*")
        }
    }
}

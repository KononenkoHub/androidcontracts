package com.example.androidcontracts.extantions

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.androidcontracts.delegates.FragmentViewBindingDelegate

fun <B : ViewBinding> Fragment.viewBindings(binder: (View) -> B): FragmentViewBindingDelegate<B> {
    return FragmentViewBindingDelegate(this, binder)
}